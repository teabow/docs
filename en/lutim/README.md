# Framapic

[Framapic](https://framapic.org) is a free and open source service which allows you to share pictures in a secure and confidential way.

  1. Paste the image you want to share.
  2. If needed, define the retention policy.
  3. You can then share the link you are given with other people for them to see the picture.


Your pictures are encrypted and stored on our servers. We cannot see the content of your files nor decrypt them.
This service is based on a free software: [Lutim](https://lut.im/).

Thanks to a new feature added by its developer, [Framapic](https://framapic.org) now allows you to easily create an image gallery! Find out how in our [example](galerie.md).

## Video Tutorial

<div class="text-center">
  <p><video controls="controls" preload="none" poster="https://framatube.org/images/media/989l.jpg" height="340" width="570">
      <source src="https://framatube.org/blip/framapic.mp4" type="video/mp4">
      <source src="https://framatube.org/blip/framapic.webm" type="video/webm">
  </video></p>
  <p>→<a href="https://framatube.org/blip/framapic.webm">webm format</a></p>
</div>

Tutorial made by [arpinux](http://arpinux.org/), *landscape architect* of GNU/Linux beginners linux distribution [HandyLinux](https://handylinux.org/).
