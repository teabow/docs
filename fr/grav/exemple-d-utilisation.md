<p class="alert alert-warning">La documentation est en cours de rédaction.<br>
Si vous souhaitez apporter des compléments, vous pouvez contribuer
sur <a href="https://framagit.org/framasoft/docs">le dépôt git</a>,
soit en ouvrant une « issue » décrivant les modifications,
soit en proposant une « merge request ».</p>
