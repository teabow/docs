# Astuces

## Je veux afficher des gifs directement dans Framateam

Pour cela, vous devez créer une commande slash.

Rendez vous dans le menu > **Intégrations** > **Commande slash** > **Ajouter une commande slash** :

  * **Titre** : Recherche et affiche un gif de Giphy
  * **Description** : Permet d'afficher un gif depuis le site giphy directement dans le salon
  * **Mot-clé de déclenchement** : gif
  * **URL de requête** : `https://giphy.framasoft.org`
  * **Méthode de requête** : POST
  * **Auto-complétion** cochée
  * **Indice de l’auto-complétion** : [texte de recherche]
  * **Description de l'auto-complétion** : Permet d'afficher un gif depuis le site giphy directement dans le salon

Ensuite, cliquez sur  **Enregistrer** puis **Effectué** puis **Retour sur Framateam**.

Désormais, vous pouvez taper la commande `/gif hello` puis `Entrée` pour faire apparaître un gif en rapport avec la demande `Hello` depuis le site giphy directement dans le salon. Celui-ci étant en anglais, les textes de recherches doivent se faire en anglais.
