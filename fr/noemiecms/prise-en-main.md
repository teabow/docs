<p class="alert alert-warning">La documentation est en cours de rédaction.<br>
Si vous souhaitez apporter des compléments, vous pouvez contribuer
sur <a href="https://framagit.org/framasoft/docs">le dépôt git</a>,
soit en ouvrant une « issue » décrivant les modifications,
soit en proposant une « merge request ».</p>

# Prise en main

## Ajouter un module au menu

Pour ajouter un raccourci dans la barre de menu tout en haut de la page, vous devez :

  * cliquer sur l'icône en forme de clé à molette présente quand vous survolez le module
  ![option module img](images/pnc_option_module.png)
  * cliquer sur

  ![ajout icône menu img](images/pnc_ajout_raccourci_menu.png)
  * donner le nom à afficher dans le menu et cliquer sur **AJOUTER AU MENU**
  ![image ajout menu modale](images/pnc_ajout_raccourci_modale.png)
