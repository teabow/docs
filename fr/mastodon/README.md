# Framapiaf

[Framapiaf](https://framapiaf.org) est un service de microblog similaire à Twitter.
Il est libre, décentralisé et fédéré. Il permet de courts messages (max. 500 caractères),
de définir leur degré de confidentialité et de suivre les membres du
réseau sans publicité ni pistage.

Le service repose sur le logiciel libre [Mastodon](https://github.com/tootsuite/mastodon/).

Voici une documentation pour vous aider à l'utiliser.
Il s’agit de la traduction française de la [documentation officielle](https://github.com/tootsuite/mastodon/tree/master/docs/Using-Mastodon) réalisée par l'équipe [Framalang](https://participer.framasoft.org/traduction-rejoignez-framalang/).

---

Guide d'utilisation de Mastodon
=====================

## Introduction

Mastodon est un logiciel de réseau social qui repose sur le protocole OStatus. Il ressemble beaucoup aux autres réseaux sociaux, notamment à Twitter, avec une différence fondamentale : il est *open source*, de sorte que tout un chacun peut installer son propre serveur (qu'on appelle aussi une « *instance* »), et les utilisateurs de chaque instance peuvent interagir librement avec ceux des autres instances (dont l'ensemble est appelé « *la fédération* »). Ainsi, il est possible à de petites communautés d'installer leur propre serveur pour interagir entre elles, tout en permettant aussi l'interaction avec d'autres communautés.

#### Décentralisation et fédération

Mastodon est un système décentralisé grâce au concept nommé « fédération ». Plutôt que de ne dépendre que d’une seule personne ou organisation pour maintenir l’infrastructure, n'importe qui peut télécharger et exécuter ce logiciel sur son propre serveur. Le terme « fédération » signifie que les divers serveurs Mastodon peuvent interagir entre eux en toute transparence, comme le font les serveurs de courriels, par exemple.

Ainsi, n'importe qui peut télécharger et faire tourner Mastodon pour une petite communauté, mais tous les utilisateurs enregistrés sur cette instance peuvent suivre, envoyer et lire les messages, appelés le plus souvent des « pouets », venus des autres instances Mastodon (ainsi que des serveurs exécutant d'autres services compatibles avec OStatus, comme GNU Social et postActiv). Cela signifie non seulement que les données des utilisateurs n’appartiennent pas à une entreprise dont l’intérêt est de les vendre à des annonceurs, mais également que si n'importe lequel des serveurs s'éteint, ses utilisateurs peuvent en installer un nouveau ou migrer vers une autre instance, plutôt que de perdre le service tout entier.

Au sein de chaque instance Mastodon, les pseudos des utilisateurs se présentent simplement sous la forme @pseudo, comme c'est le cas sur d'autres services tels que Twitter. Les utilisateurs d'autres instances apparaissent, et peuvent être recherchés et suivis en tant que `@pseudo@nomduserveur.ext` (par exemple,  `@gargron` de l'instance `mastodon.social` peut être suivi depuis d'autres instances en tant que `@gargron@mastodon.social`.

Les messages d'utilisateurs d'instances extérieures sont *fédérés* dans l'instance locale, par exemple si `utilisateur1@mastodon1` suit `utilisateur2@gnusocial2`, n'importe quel message de `utilisateur2@gnusocial2` apparaît à la fois dans la page d'accueil de `utilisateur1@mastodon1` et dans le fil d'actualités public du serveur `mastodon1`. Les administrateurs des serveurs Mastodon ont un certain contrôle dessus et peuvent exclure des messages d'utilisateurs du fil d'actualités public; les paramètres de confidentialité des messages des utilisateurs de Mastodon ont aussi leur influence, voir la section [Confidentialité du pouet](User-guide.html#toot-privacy).

* [Premiers pas](User-guide.html#premiers-pas)
  * [Paramétrer votre profil](User-guide.html#paramétrer-votre-profil)
  * [Notifications e-mail](User-guide.html#notifications-e-mail)
  * [Poster du texte](User-guide.html#publier-un-message)
    * [Avertissement](User-guide.html#avertissement-sur-le-contenu)
    * [Hashtags](User-guide.html#hashtags)
    * [Partages et Favoris](User-guide.html#partages-et-favoris)
  * [Poster des images](User-guide.html#poster-des-images)
  * [Suivre d'autres utilisateurs](User-guide.html#suivre-d-autres-utilisateurs)
  * [Notifications](User-guide.html#notifications)
  * [Applis mobiles](User-guide.html#applis-mobiles)
  * [Le fil public global](User-guide.html#le-fil-public-global)
  * [Le fil public local](User-guide.html#le-fil-public-local)
  * [Faire une recherche](User-guide.html#faire-une-recherche)
* [Confidentialité, sûreté et sécurité](User-guide.html#confidentialité-sûreté-et-sécurité)
  * [Authentification à deux facteurs](User-guide.html#authentification-à-deux-facteurs)
  * [Confidentialité du compte](User-guide.html#confidentialité-du-compte)
  * [Confidentialité des pouets](User-guide.html#confidentialité-des-pouets)
  * [Bloquer](User-guide.html#bloquer)
  * [Signaler des pouets ou des utilisateurs](User-guide.html#signaler-des-pouets-ou-des-utilisateurs)
  * [Crédits](User-guide.html#crédits)